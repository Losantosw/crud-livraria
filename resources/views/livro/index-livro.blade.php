<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Crud Livraria</title>
</head>
<body>
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nome</th>
          <th scope="col">Autor</th>
          <th scope="col">Ano Lançamento</th>
          <th scope="col">
            <a class="btn btn-success" href="{{route('livro.create')}}">Adicionar Livro</a>
          </th>
          <th>
            <a class="btn btn-info" href="{{route('home')}}">Voltar</a>
          </th>
        </tr>
      </thead>
      <tbody>
        @foreach($livros as $livro)
          <tr>
            <th scope="row">{{$livro->id}}</th>
            <td>{{$livro->nome}}</td>
            <td>{{$livro->autor}}</td>
            <td>{{$livro->anolancamento}}</td>
            <td>
              <a class="btn btn-warning" href="{{ route('livro.edit', $livro->id) }}">Editar</a>
            </td>
            <td>
              <form method="POST" action="{{route('livro.destroy', $livro->id)}}">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger">Deletar</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
</body>
</html>
