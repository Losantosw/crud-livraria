<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Crud Livraria</title>
</head>
<body>
  <div class="container">
    <div class="">
      <form action="{{route('livro.store')}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}

          <div class="col-md-6">
            <div class="form-group">
              <h1>Cadastrar Livro</h1>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="nome">Nome</label>
              <input type="text" class="form-control" id="nome" placeholder="Nome do livro" name="nome">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="autor">Autor</label>
              <input type="text" class="form-control" id="autor" placeholder="Autor do livro" name="autor">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="anolancamento">Ano Lançamento</label>
              <input type="text" class="form-control" id="anolancamento" placeholder="Ano lançamento" name="anolancamento">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <button type="submit" class="btn btn-success">Salvar</button>
              <button class="btn btn-info" href="{{route('livro.index')}}">Voltar</button>
            </div>
          </div>
      </form>
    </div>
  </div>
</body>
</html>
